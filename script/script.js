// ## Теоретичне питання
// 1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript

// Кожен об'єкт має внутрішнє посилання на інший об'єкт - його прототип. Коли властивість запитується з об'єкту 
// і якщо вона не знайдена в самому об'єкті, JavaScript продовжить пошук в його прототипі, і так далі, до тих пір, 
// поки властивість не буде знайдена або ж буде досягнемо кінця ланцюжка прототипів.

// 2. Для чого потрібно викликати super() у конструкторі класу-нащадка?

// Виклик super() у конструкторі класу-нащадка потрібен для того, щоб ініціалізувати базовий клас або клас-батько до того, 
// як будь-які інші операції будуть виконані в класі-нащадку.
// Коли ви визиваєте super() у конструкторі класу-нащадка, виклик іде до конструктора класу-батька і передає всі аргументи, 
// що були передані в конструктор класу-нащадка. Це дозволяє базовому класу виконати необхідні ініціалізації, 
// які можуть бути важливими для коректної роботи класу-нащадка.





class Employee {
    constructor(name, age, salary){
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    get newName(){
        return this.name;
    }
    set newName(value){
         this.name = value;
    }
    get newAge(){
        return this.age;
    }
    set newAge(value){
        this.age = value;
    } 
    get newSalary(){
        return this.salary;
    }
    set newSalary(value){
         this.salary = value;
    }  
    
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
    
  }
  
  get newSalary() {
    
    return this.salary*3;
  }
  
}

const prog1 = new Programmer("Vasya", 30, 1500, ["Python", "JS"]);


console.log(prog1);
console.log(prog1.newSalary);




const prog2 = new Programmer("Olena", 25, 2000, ["Python", "JS","PHP"]);

console.log(prog2);
console.log(prog2.newSalary);

const prog3 = new Programmer("Mike", 35, 3000, ["C++", "Java", "PHP"]);

console.log(prog3);
console.log(prog3.newSalary);

const prog4 = new Programmer("Mari", 28, 2200, ["PL SQL", "Java", "PHP"]);

console.log(prog4);
console.log(prog4.newSalary);

const prog5 = new Programmer("Jun", 30, 5000, ["JS", "HTML"]);

console.log(prog5);
console.log(prog5.newSalary);
